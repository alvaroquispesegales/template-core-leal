import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './routes'

const app = createApp(App)

app.use(createPinia())
app.use(router)

// para estilos => https://primevue.org
import PrimeVue from 'primevue/config';
import "primevue/resources/themes/lara-light-indigo/theme.css"; // para estilo en componentes de vueprime
import "primeflex/primeflex.css" // para estilos css
import 'primeicons/primeicons.css'; // para iconos
app.use(PrimeVue, );

app.mount('#app')
