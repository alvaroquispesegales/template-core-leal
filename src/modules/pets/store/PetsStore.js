import { defineStore } from "pinia";
import axios from "axios";
import { headerConfig } from "@/helpers/token";
const api_leal = import.meta.env.VITE_API_LEAL;

export const usePetsStore = defineStore("PetsStore", () => {

  const getAllPets = async () => {
    try {
      const res = await axios.get(`${api_leal}/v1/pets`);
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }

  const savePet = async (data) => {
    try {
      const res = await axios.post(`${api_leal}/v1/pets`, data, headerConfig());
      return res.data;
    } catch (error) {
      console.log('error', error)
      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }

  const updatePet = async (data) => {}

  return { getAllPets, savePet, updatePet }
})