export default [
  {
    path: "/users",
    name: "users",
    component: () => import("@/modules/users/views/UsersView.vue")
  }
]