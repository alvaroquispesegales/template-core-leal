import { defineStore } from "pinia";
import axios from "axios";
import { headerConfig } from "@/helpers/token";
const api_leal = import.meta.env.VITE_API_LEAL;

export const useUsersStore = defineStore("UsersStore", () => {

  const getAllUsers = async () => {
    try {
      const res = await axios.get(`${api_leal}/v1/users`);
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }

  return { getAllUsers }  
})