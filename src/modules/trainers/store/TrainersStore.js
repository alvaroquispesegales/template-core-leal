import { defineStore } from "pinia";
import axios from "axios";
import { headerConfig } from "@/helpers/token";
const api_leal = import.meta.env.VITE_API_LEAL;

export const useTrainersStore = defineStore("TrainersStore", () => {

  const getAllTrainers = async () => {
    try {
      const res = await axios.get(`${api_leal}/v1/trainers`);
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }

  const saveTrainer = async (data) => {
    try {
      const res = await axios.post(`${api_leal}/v1/trainers`, data, headerConfig());
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "Local error: " + error
      }
    }
  }

  return { getAllTrainers, saveTrainer }
})