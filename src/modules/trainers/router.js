export default [
  {
    path: "/trainers",
    name: "trainers",
    component: () => import("@/modules/trainers/views/TrainersView.vue")
  }
]