
export default [
  {
    path: "/events",
    name: "eventList",
    component: () => import("@/modules/events/views/EventsView.vue")
  }
]