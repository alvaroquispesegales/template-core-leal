import axios from "axios";
import { defineStore } from "pinia";
import { headerConfig } from "@/helpers/token";
const api_leal = import.meta.env.VITE_API_LEAL;

export const useEventStore = defineStore('EventStore', () => {
  const getEvents = async () => {
    try {
      const res = await axios.get(`${api_leal}/v1/events`);
      return res.data;
    } catch (error) {
      return {
        errorCode: "-1",
        message: "local error: " + error
      }
    }
  }

  const saveEvent = async (data) => {
    try {
      const res = await axios.post(`${api_leal}/v1/events`, data, headerConfig());
      return res.data;
    } catch (error) {
      return {
        errorCode: "-1",
        message: "Local error: " + error
      }
    }
  }

  return { getEvents, saveEvent }
})