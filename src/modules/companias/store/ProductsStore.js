import { defineStore } from 'pinia';
import { headerConfig } from '@/helpers/token'
import axios from 'axios'
const api_leal = import.meta.env.VITE_API_LEAL;

export const useProductsStore = defineStore('ProductStore', () => {

  const getProductsByCompany = async (params) => {
    try {
      const res = await axios.post(`${api_leal}/v1/products/search`, params);
      return res.data;
    } catch (error) {
      return {
        errorCode: "-1",
        message: "Local error: " + error
      }
    }
  }

  const saveProduct = async (data) => {
    try {
      const res = await axios.post(`${api_leal}/v1/products`, data, headerConfig());
      return res.data;
    } catch (error) {
      return {
        errorCode: "-1",
        message: "Local error: " + error
      }
    }
  }

  return { getProductsByCompany, saveProduct }
});