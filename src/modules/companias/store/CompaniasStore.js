import { defineStore } from 'pinia'
import { headerConfig } from '@/helpers/token';
import axios from 'axios';
const api_leal = import.meta.env.VITE_API_LEAL;
export const useCompaniasStore = defineStore('CompaniasStore', () => {

  const obtenerCompaniasTodos = async () => {
    try {
      const r = await axios.get(`${api_leal}/v1/companies`);
      return r.data;
    } catch (error) {
      return {
        codigoMensaje: "-1",
        mensaje: "error local: " + error
      }
    }
  }
  const registrarCompania = async (obj) => {

    try {
      const r = await axios.post(`${api_leal}/v1/companies`, obj,headerConfig());
      return r.data;
    } catch (error) {
      return {
        codigoMensaje: "-1",
        mensaje: "error local: " + error
      }
    }
  }
  const modificarCompania = async (obj) => {
    try {
      const r = await axios.put(`${api_leal}/v1/companies`, obj,headerConfig());
      return r.data;
    } catch (error) {
      return {
        codigoMensaje: "-1",
        mensaje: "error local: " + error
      }
    }
  }
  return { obtenerCompaniasTodos, registrarCompania, modificarCompania }

});
