export default [
  {
    path: '/companias/companias',
    name: 'companiasCompanias',
    component: () => import('@/modules/companias/views/CompaniasView.vue')
  },
  {
    path: '/company/products/:id',
    name: 'companyProducts',
    component: () => import('@/modules/companias/views/ProductsView.vue')
  }
]
