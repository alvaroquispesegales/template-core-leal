export default [
  {
    path: "/lost-pets",
    name: "lostPets",
    component: () => import("@/modules/lostpets/views/LostPetsView.vue")
  }
]