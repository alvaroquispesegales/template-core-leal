import { defineStore } from "pinia";
import axios from "axios";
import { headerConfig } from "@/helpers/token";
const api_leal = import.meta.env.VITE_API_LEAL;

export const useLostPetsStore = defineStore("LostPetsStore", () => {

  const getAllLostPets = async () => {
    try {
      const res = await axios.get(`${api_leal}/v1/lost-pets`);
      return res.data;
    } catch (error) {
      return {
        errorCode: "-1",
        message: "Local error: " + error
      }
    }
  }

  const savePetLost = async (data) => {
    try {
      const res = await axios.post(`${api_leal}/v1/lost-pets`, data, headerConfig());
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }
  return {
    getAllLostPets, savePetLost
  }
})