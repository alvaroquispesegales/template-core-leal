import axios from "axios";
import { defineStore } from "pinia";
const api_leal = import.meta.env.VITE_API_LEAL;

export const useUserStore = defineStore("UserStore", () => {
  const getUsers = async () => {
    try {
      const res = await axios.get(`${api_leal}/v1/users`);
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }

  return { getUsers }
})