import { defineStore } from "pinia";
import axios from "axios";
import { headerConfig } from "@/helpers/token";
const api_leal = import.meta.env.VITE_API_LEAL;

export const useCitiesStore = defineStore("CitiesStore", () => {

  const getCities = async () => {
    try {
      const res = await axios.get(`${api_leal}/v1/cities`)
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }

  return { getCities }
})