export default [
  {
    path: '/admin/users',
    name: 'adminUsers',
    component: () => import('@/modules/admin/views/UsersView.vue')
  }
]