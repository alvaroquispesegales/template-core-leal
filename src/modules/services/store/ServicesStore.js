import { defineStore } from "pinia";
import axios from "axios";
import { headerConfig } from "@/helpers/token";
const api_leal = import.meta.env.VITE_API_LEAL;

export const useServicesStore = defineStore("ServicesStore", () => {
  
  const getServicebyType = async (params) => {
    try {
      const res = await axios.post(`${api_leal}/v1/dogwalker/search`, params);
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }

  return { getServicebyType }
})