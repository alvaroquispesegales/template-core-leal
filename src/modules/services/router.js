export default [
  {
    path: "/services",
    name: "services",
    component: () => import("@/modules/services/views/ServicesView.vue")
  }
]