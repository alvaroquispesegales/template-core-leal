export default [
    {
      path: '/auth/login',
      name: 'authLogin',
      component: () => import('@/modules/auth/views/LoginFormView.vue')
    },
    {
      path: '/auth/register',
      name: 'authRegister',
      component: () => import('@/modules/auth/views/RegisterFormView.vue')
    }
]
