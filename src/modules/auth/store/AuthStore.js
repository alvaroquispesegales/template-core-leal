import axios from 'axios'
import { defineStore } from 'pinia';
import { headerConfig } from '@/helpers/token'
import Cookies from "js-cookie"


const api_leal = import.meta.env.VITE_API_LEAL;

export const useAuthStore = defineStore('AuthStore', () => {
  const onLogin = async (data) => {

    try {
      const res = await axios.post(`${api_leal}/v1/auth/login`, data);

      return res.data;
    } catch (error) {

      return {
        errorCode: -1,
        message: "local error: " + error
      }
    }
  }

  const onRegister = async (data) => {
    try {
      const res = await axios.post(`${api_leal}/v1/auth/signup`, data,headerConfig);
      return res.data;
    } catch (error) {
      return {
        errorCode: -1,
        message: "local error:" + error
      }
    }
  }

  const setToken = (token) => {
    Cookies.set("token", token);
  }

  const getToken = () => {
    return Cookies.get("token");
  }

  return { onLogin, onRegister, setToken, getToken }
});
