

// Composables
import { createRouter, createWebHistory } from 'vue-router'

// modulos
import routerAuth from '@/modules/auth/router';
import routerCompanias from '@/modules/companias/router';
import routerAdmin from '@/modules/admin/router';
import routerPets from '@/modules/pets/router';
import routerEvents from "@/modules/events/router";
import routerHome from '@/modules/home/router';
import routerTrainers from "@/modules/trainers/router";
import routerUsers from "@/modules/users/router";
import routerServices from "@/modules/services/router";
import routerLostPets from "@/modules/lostpets/router";

const baseRoutes = [];

const routesModulos = baseRoutes.concat(routerAuth,routerCompanias, routerAdmin, routerPets, routerEvents,routerHome, 
        routerTrainers, routerUsers, routerServices, routerLostPets);


const router = createRouter({
    history: createWebHistory(),
    routes:routesModulos,
})

export default router
