
export function registrarToken(token){
    localStorage.setItem('token', JSON.stringify(token).valueOf());
}
export function headerConfig(){
    const token =   localStorage.getItem('token');
    const  headers = {
        "Content-Type": "application/json;charset=UTF-8",
        Authorization: `Bearer ${token.replace(/[ '"]+/g, ' ')}`
    }
    const configuracion = {
        headers
    };
    return configuracion;
}


