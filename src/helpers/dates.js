const format_DDMMYYY = (dateString) =>{
    const date = new Date(dateString);
    return new Intl.DateTimeFormat('default', {dateStyle: 'long'}).format(date);
}
export { format_DDMMYYY }
